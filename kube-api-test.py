from kubernetes import client, config
from kubernetes.client.rest import ApiException
from pprint import pprint

from datetime import datetime
from dateutil.relativedelta import relativedelta

# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()

# v1 = client.CoreV1Api()
# print("Listing pods with their IPs:")
# ret = v1.list_pod_for_all_namespaces(watch=False)
# for i in ret.items:
#     print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))


v1 = client.CoreV1Api()

# create an instance of the API class
api = client.CustomObjectsApi()
group = 'certmanager.k8s.io' # str | the custom resource's group
version = 'v1alpha1' # str | the custom resource's version
plural = 'certificates' # str | the custom object's plural name. For TPRs this would be lowercase plural kind.
name = 'boomer' # str | the custom object's name

for ns in v1.list_namespace().items:
    print("Discovering certificates in {}".format(ns.metadata.name))
    try:
        cert_response = api.list_namespaced_custom_object(group=group, version=version, namespace=ns.metadata.name, plural=plural)
        # Custom object responses don't appear to be well formed objects...
        for cert in cert_response['items']:
            print ("Found Certificate {}, expiry {}".format ( cert['metadata']['name'], cert['status']['notAfter']))
            expiry_str = cert['status']['notAfter']
            expires = datetime.strptime(expiry_str, '%Y-%m-%dT%H:%M:%SZ')
            now =  datetime.now()

            t = expires - now
            seconds = t.total_seconds()
            hours = (seconds / (60*60))
            mins  = (seconds /60) % 60
            seconds = seconds % 60
            renewBefore = "{}h{}m{}s".format( int(hours), int(mins), int(seconds))

            print("Renewal should be set at {}".format(renewBefore))


        # pprint(api_response)
    except ApiException as e:
        print("Exception when calling CustomObjectsApi->get_cluster_custom_object: %s\n" % e)